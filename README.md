# Sundries: Expense tracking API

## Overview

This app is an API based on [Flask](https://palletsprojects.com/p/flask/) to
provide a basic *expense tracker* and logging app.

## Endpoints

The following endpoints are available from the app:

### Transactions

An individual transaction is called a *bit*.

| Action   | Method | Endpoint |
|----------|--------|----------|
| Create   | `POST` | `/api/v1/bits` |
| List     | `GET`  | `/api/v1/bits` |
| Retrieve | `GET`  | `/api/v1/bits/<id:number>` |
| Update   | `PUT`  | `/api/v1/bits/<id:number>` |
| Delete   | `POST` | `/api/v1/bits/<id:number>/delete` |

### Tags

Tags are used to classify different types on expenses. They are used to
populate menus in the front-end.

| Action   | Method | Endpoint |
|----------|--------|----------|
| Create   | `POST` | `/api/v1/tag` |
| List     | `GET`  | `/api/v1/tag` |
| Update   | `PUT`  | `/api/v1/tag/<id:number>` |
| Delete   | `POST` | `/api/v1/tag/<id:number>/delete` |

### Accounts

Accounts are used to identify different expense accounts. They are used to
populate menus in the front-end.

| Action   | Method | Endpoint |
|----------|--------|----------|
| Create   | `POST` | `/api/v1/account` |
| List     | `GET`  | `/api/v1/account` |
| Update   | `PUT`  | `/api/v1/account/<id:number>` |
| Delete   | `POST` | `/api/v1/account/<id:number>/delete` |

## License

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.