#!env/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from flask import (
    Flask,
    jsonify,
    request,
    abort,
    make_response,
    url_for
)


API_ROOT = "/api/v1/"


app = Flask(__name__)


users = [
    {
        'id': 1,
        'email': 'rod.t.manning@gmail.com',
        'name': 'Rod Manning'
    }
]


rows = [
 {
     'id': 1,
     'user': users[0],
     'tag': 'coffee',
     'account': 'savings',
     'text': 'Morning doppio',
     'amount': 4.0,
 },
 {
     'id': 2,
     'user': users[0],
     'tag': 'groceries',
     'account': 'savings',
     'text': 'Weekly shop',
     'amount': 34.50,
 },
 {
     'id': 3,
     'user': users[0],
     'tag': 'dog',
     'account': 'savings',
     'text': 'Dog treats =)',
     'amount': 77.23,
 }
]


tags = [
    {
        'name': 'coffee',
        'is_active': True
    },
    {
        'name': 'dog',
        'is_active': True
    },
    {
        'name': 'groceries',
        'is_active': True
    }
]


accounts = [
    {
        'name': 'savings',
        'is_active': True
    },
    {
        'name': 'cheque',
        'is_active': True
    }
]


def make_public(obj):
    """Replace an `id` with an `uri` in an object."""
    new_obj = {}
    for field in obj:
        if field == "id":
            uri = url_for('get_bit', idx=obj['id'], _external=True)
            new_obj["uri"] =  uri
        else:
            new_obj[field] = obj[field]
    return new_obj


# List all bits (transactions)
@app.route(API_ROOT + "bits", methods=["GET"])
def bits_index():
    data = [make_public(bit) for bit in rows]
    return jsonify({"data": data})


# Create new bit
@app.route(API_ROOT + "bits", methods=["POST"])
def create_bit():
    # Ensure data was passed to create the obj
    if not request.json:
        abort(400)
    # Ensure required fields were passed
    required_fields = [
        "account", "text", "amount",
        #"user"
    ]
    for field in required_fields:
        if field not in request.json:
            abort(400)
    # Create the new object
    bit = {
        'id': rows[-1]['id'] + 1,
        # TODO: User == Logged in User
        'user': users[0],
        'tag': request.json.get("tag", ""),
        'account': request.json["account"],
        'text': request.json.get("text", ""),
        'amount': request.json["amount"]
    }
    rows.append(bit)
    return jsonify({"data": make_public(bit)}), 201


# Retrieve an individual bit
@app.route(API_ROOT + "bits/<int:idx>", methods=["GET"])
def get_bit(idx):
    bit = [row for row in rows if row["id"] == idx]
    if len(bit) == 0:
        abort(404)
    return jsonify({"data": bit[0]})


# Update an individual bit
@app.route(API_ROOT + "bits/<int:idx>", methods=["PUT"])
def update_bit(idx):
    bit = [row for row in rows if row["id"] == idx]
    # Make sure the bit exists
    if len(bit) == 0:
        abort(404)
    # Filter data fields
    ## TODO...
    for field in request.json:
        bit[0][field] = request.json[field]
    return jsonify({"data": make_public(bit[0])})


# Delete a bit
@app.route(API_ROOT + "bits/<int:idx>/delete", methods=["DELETE"])
def delete_bit(idx):
    bit = [row for row in rows if row["id"] == idx]
    # Make sure the bit exists
    if len(bit) == 0:
        abort(404)
    # Delete the object
    rows.move(bit[0])
    return jsonify({"data": True})


# Error handler (404)
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


if __name__ == "__main__":
    app.run(debug=True)
